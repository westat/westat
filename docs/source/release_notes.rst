发布日志
=====

.. _release_notes:

Version 0.2.3 (2023-06-29)
------------

修改:

1、woe_iv 函数新增optbinning 分箱，可选设置最大分箱数，并设置分箱单调性


Version 0.2.2 (2023-06-28)
------------

修改:

1、根据sphinx构建并完善产品文档


