westat: 金融统计工具箱
===================================

**westat** 是一个用于金融行业信用风险模型开发的python库，其中严格遵循 ``SEMMA`` 数据分析方法论，此外，我们在westat中也增加了一些常用的数据分析、统计、量化等相关内容。  `westat <http://www.pyminer.com/>`_ 致力于提供 *简单* 且
*高效* 的统计工具箱.

.. note::

   westat 致力于更好的金融统计!

目录
--------

.. toctree::
   :maxdepth: 1
   :caption: 安装

   installation

.. toctree::
   :maxdepth: 1
   :caption: 快速开始

   tutorials/tutorials_version

.. toctree::
   :maxdepth: 1
   :caption: 数据准备

   tutorials/tutorials_sample

.. toctree::
   :maxdepth: 1
   :caption: 数据探索

   tutorials/tutorials_explore

.. toctree::
   :maxdepth: 1
   :caption: 数据处理

   tutorials/tutorials_modify

.. toctree::
   :maxdepth: 1
   :caption: 模型开发

   tutorials/tutorials_model

.. toctree::
   :maxdepth: 1
   :caption: 模型评估

   tutorials/tutorials_assess

.. toctree::
   :maxdepth: 1
   :caption: 量化分析

   quant/quant

.. toctree::
   :maxdepth: 1
   :caption: 工具函数

   utils/utils

.. toctree::
   :maxdepth: 1
   :caption: 获取帮助

   help/faq
   help/how_to_get_support

.. toctree::
   :maxdepth: 1
   :caption: 发布日志

   release_notes
